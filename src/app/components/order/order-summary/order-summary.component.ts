import {Component, OnInit} from '@angular/core';
import { ShoppingCartService } from '../../restaurants/restaurant-detail/shopping-cart/shopping-cart.service';
import { OrderService } from '../order.service';

@Component({
    selector: 'lacc-order-summary',
    templateUrl: './order-summary.component.html'
})
export class OrderSummaryComponent implements OnInit {

    constructor(private orderService:OrderService) {
    }

    ngOnInit() {
    }
    items(){
        console.log(this.orderService.getOrderItems());
      this.orderService.getOrderItems(); 
    }
    total(){
        console.log(this.orderService.itemsValue()+10);
        this.orderService.itemsValue() + 10;
    }
}
