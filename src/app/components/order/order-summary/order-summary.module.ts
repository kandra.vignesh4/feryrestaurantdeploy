import { CommonModule } from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { TextMaskModule } from 'angular2-text-mask';
import { SharedModule } from '../../shared/shared.module';
import {OrderSummaryComponent} from './order-summary.component';


const ROUTES: Routes = [
    {path: '', component: OrderSummaryComponent}
];

@NgModule({
    declarations: [OrderSummaryComponent],
    imports: [SharedModule,CommonModule,TextMaskModule,RouterModule.forChild(ROUTES)]
})

export class OrderSummaryModule {

}
